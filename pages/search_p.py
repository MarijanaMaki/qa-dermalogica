import time
from selenium.webdriver.common.by import By
from base.selenium_driver import SeleniumDriver

#*******locators*********
search_bar="//input[@placeholder='Produkt suchen']"
all_results="//div[contains(text(),'Alle Resultate')]"
no_result_message="//p[@class='u-lead u-lead--bold u-uppercase']"
empty_string=''


class searchClass(SeleniumDriver):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    def search_bar(self):
        return self.driver.find_element(By.XPATH, search_bar)

    def type_search_bar(self, search):
        self.search_bar().send_keys(search)

    def get_all_results(self):
        return self.driver.find_element(By.XPATH, all_results)

    def click_all_results(self):
        self.elementClick(all_results, locatorType="xpath")


    def searchB(self, search):
        self.type_search_bar(search)
        self.click_all_results()

    def verifySearchSuccessful(self):
        result = self.isElementPresent(no_result_message, byType="xpath")
        return result
