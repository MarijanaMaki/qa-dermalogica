import time
from selenium.webdriver.common.by import By
from base.selenium_driver import SeleniumDriver



#***********locators**************
daily_skin_healt="//h3[contains(text(),'Daily Skin Health')]"
in_den_warenkorb="//div[@class='products-list__products']//div[1]//div[1]//div[2]//div[2]//button[1]"
in_den_warenkorb_button2="//button[@class='button-filled blue-filled']"
cart_icon="//div[@class='cart-icon']//div//*[@class='injected-svg']"
zur_kasse_button="//button[contains(@class,'button-filled blue-filled')]"

#******* new_address_locators **********#
new_adress="//div[@class='address-picker__address address-picker__address--add-new']"
first_name="//input[@name='firstName']"
last_name="//input[@name='lastName']"
street_name=" //input[@name='streetAddress1']"
company_name="//input[@name='companyName']"
postal_code="//input[@name='postalCode']"
city_name="//input[@name='city']"
region="//input[@name='countryArea']"
email_address="//input[contains(@name,'email')]"
phone_number="//input[@name='phone']"
save_address="//button[contains(@class,'button-filled blue-filled sc-htoDjs iEdzXj')]"

versand_button="//button[contains(@class,'button-filled blue-filled')]"
continue_button="/button[contains(@class,'button-filled blue-filled mt-30')]"  # (Rechnungsadresse)
visa_card="//label[contains(text(),'Visa')]"
bestelung_button="//button[contains(@class,'button-filled blue-filled')]"
check_order_page= "//h3[contains(@class,'checkout__header lower')]"



class checkOut(SeleniumDriver):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver


    def daily_skin(self):
         return self.driver.find_element(By.XPATH, daily_skin_healt)

    def click_on_daily_skin(self):
        self.elementClick(daily_skin_healt, locatorType="xpath")

    def in_den_warenkorb(self):
        return  self.driver.find_element(By.XPATH, in_den_warenkorb)

    def click_in_den_warenkorb(self):
        self.elementClick(in_den_warenkorb, locatorType="xpath")

    def in_den_war_button2(self):
        return self.driver.find_element(By.XPATH, in_den_warenkorb_button2)

    def click_in_den_war_button2(self):
        self.elementClick(in_den_warenkorb_button2, locatorType="xpath")

    def cart_icon(self):
        return self.driver.find_element(By.XPATH, cart_icon)

    def click_on_cart_icon(self):
        self.elementClick(cart_icon, locatorType="xpath")

    def zur_kasse_button(self):
        return  self.driver.find_element(By.XPATH, zur_kasse_button)

    def click_on_zur_kasse(self):
        self.elementClick(zur_kasse_button, locatorType="xpath")

    def new_adress_button(self):
        return self.driver.find_element(By.XPATH, new_adress)

    def click_on_new_adress(self):
        self.elementClick(new_adress, locatorType="xpath")


    def first_name(self):
        return self.driver.find_element(By.XPATH, first_name)

    def type_name(self,name):
        self.first_name().send_keys(name)

    def last_name(self):
        return self.driver.find_element(By.XPATH, last_name)

    def type_last_name(self,lastName):
        self.last_name().send_keys(lastName)

    def street_name(self):
        return self.driver.find_element(By.XPATH, street_name)

    def type_street_name(self, street):
        self.street_name().send_keys(street)

    def postal_code(self):
        return self.driver.find_element(By.XPATH, postal_code)

    def type_postal_code(self,code):
        self.postal_code().send_keys(code)

    def city_name(self):
        return self.driver.find_element(By.XPATH, city_name)

    def type_city_name(self,city):
        self.city_name().send_keys(city)

    def email_address(self):
        return self.driver.find_element(By.XPATH, email_address)

    def type_email_address(self,email):
        self.email_address().send_keys(email)

    def phone_number(self):
            return self.driver.find_element(By.XPATH, phone_number)

    def type_phone_number(self, phone):
            self.phone_number().send_keys(phone)

    def save_address(self):
            return self.driver.find_element(By.XPATH, save_address)

    def click_save_address(self):
            self.elementClick(save_address, locatorType="xpath")

    def continue_button(self):
        return self.driver.find_element(By.XPATH, continue_button)

    def click_continue_button(self ):
        self.elementClick(continue_button, locatorType="xpath")

    def visa_card(self):
            return self.driver.find_element(By.XPATH, visa_card)

    def click_visa_card(self):
            self.elementClick(visa_card, locatorType="xpath")

    def bestelung_button(self):
        return self.driver.find_element(By.XPATH, bestelung_button)

    def click_bestelung_button(self):
        self.elementClick(bestelung_button, locatorType="xpath")


    def put_product(self):
        self.click_on_daily_skin()
        self.click_in_den_warenkorb()
        time.sleep(2)
        self.click_in_den_war_button2()


    def go_to_cart(self):
        self.click_on_cart_icon()
        self.click_on_zur_kasse()
        time.sleep(2)


    def new_address_def(self,name,lastName,street,code,city,email,phone):
        self.click_on_new_adress()
        time.sleep(2)
        self.type_name(name)
        self.type_last_name(lastName)
        self.type_street_name(street)
        self.type_postal_code(code)
        self.type_city_name(city)
        self.type_email_address(email)
        self.type_phone_number(phone)
        time.sleep(2)
        self.click_save_address()
        time.sleep(2)


    def continue_button_d(self):
        self.click_continue_button()

    def bestelung(self):
        self.click_bestelung_button()

    def verifyCheckOrderSuccessful(self):
        result = self.isElementPresent(check_order_page, byType="xpath")
        return result


    def get_versand(self):
        return self.driver.find_element(By.XPATH, versand_button)

    def click_on_versand(self):
        self.elementClick(versand_button, locatorType="xpath")
