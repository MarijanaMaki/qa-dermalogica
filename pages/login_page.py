import time
from selenium.webdriver.common.by import By
from base.selenium_driver import SeleniumDriver

#*******locators*********

user_icon= "//li[@class='main-menu__icon']//div//div//*[@class='injected-svg']"
email_input="//div[@class='input__content']//input[@name='email']"
pass_input="//input[@name='password']"
sing_in_button="//button[contains(@class,'button-filled blue-filled font-mod')]"

class loginPage(SeleniumDriver):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    def get_user_icon(self):
        return self.driver.find_element(By.XPATH, user_icon )

    def click_on_user_icon(self):
        self.elementClick(user_icon, locatorType="xpath")

    def get_email_field(self):
        return self.driver.find_element(By.XPATH, email_input)

    def type_email(self, email):
        self.get_email_field().send_keys(email)

    def get_pass_field(self):
        return self.driver.find_element(By.XPATH, pass_input)

    def type_pass(self, password):
        self.get_pass_field().send_keys(password)

    def get_sing_in_button(self):
        return self.driver.find_element(By.XPATH, sing_in_button)

    def click_on_sing_in_button(self):
         self.elementClick(sing_in_button, locatorType="xpath")

    def login(self,email, password):
        self.click_on_user_icon()
        self.type_email(email)
        self.type_pass(password)
        self.click_on_sing_in_button()
