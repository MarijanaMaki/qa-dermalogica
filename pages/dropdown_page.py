import time

from selenium.webdriver.common.by import By
from base.selenium_driver import SeleniumDriver

#*******Locators**********
fur_institute="// div[contains(text(), 'Für Institute')]"
fur_prof_institute="//a[contains(text(),'Für prof. Institute')]"
grundkonzepte="//a[contains(text(),'Grundkonzepte')]"
preisg_bildung="//a[contains(text(),'Preisgekrönte bildung')]"
rev_produkte=" //a[contains(text(),'Revolutionäre Produkte')]"
inov_produkte="//a[contains(text(),'Innovative Behandlungen')]"
buiss_unter="//a[contains(text(),'Business Unterstützung')]"
institut_finder="//a[contains(text(),'Institut finder')]"
search_inst_finder="//input[@id='store-finder-search-input']"

class Dropdown(SeleniumDriver):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver


    def get_fur_institute(self):
        return self.driver.find_element(By.XPATH, fur_institute)

    def get_grundkonzepte(self):
        return self.driver.find_element(By.XPATH, grundkonzepte)

    def get_preisg_bildung(self):
        return self.driver.find_element(By.XPATH, preisg_bildung)

    def get_rev_produkte(self):
        return self.driver.find_element(By.XPATH, rev_produkte)

    def get_inov_produkte(self):
        return self.driver.find_element(By.XPATH, inov_produkte)

    def get_buiss_unter(self):
        return self.driver.find_element(By.XPATH, buiss_unter)

    def get_institut_finder(self):
        return self.driver.find_element(By.XPATH, institut_finder)

    def click_institut_finder(self):
        self.elementClick(institut_finder, locatorType="xpath")

    def search_inst_finder(self):
            return self.driver.find_element(By.XPATH, search_inst_finder)

    def type_search_inst_finder(self,search):
        self.search_inst_finder().send_keys(search)

    def finder(self, search):
       self.click_institut_finder()
       time.sleep(3)
       self.type_search_inst_finder(search)


