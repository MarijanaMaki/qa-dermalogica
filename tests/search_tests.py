import time
from selenium import webdriver
from pages.login_page import loginPage
from pages.checkout_p import checkOut
from pages.search_p import searchClass, empty_string
import pytest


import unittest

class TestCheckout(unittest.TestCase):
          baseURL = "https://dermalogica-staging.mindnow.io"
          driver = webdriver.Chrome("./driver/chromedriver 2")
          driver.implicitly_wait(3)
          driver.maximize_window()
          lp = loginPage(driver)
          ch = checkOut(driver)
          sc =searchClass(driver)

          @pytest.mark.run(order=1)
          def test_search(self):
              self.driver.get(self.baseURL)
              time.sleep(4)
              self.lp.login("Marijana@mindnow.io", "Mara1991")
              self.sc.searchB('mask')

          @pytest.mark.run(order=2)
          def test_search2(self):
              self.driver.get(self.baseURL)
              time.sleep(3)
              self.sc.searchB('fsdfs')

              result = self.sc.verifySearchSuccessful()
              assert result == True

          @pytest.mark.run(order=3)
          def test_search3(self):
              self.driver.get(self.baseURL)
              time.sleep(3)
              self.sc.searchB(empty_string)




#py.test -s -v tests/search_tests.py



