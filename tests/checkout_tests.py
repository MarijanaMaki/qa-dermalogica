import time

import pytest
from selenium import webdriver
from pages.login_page import loginPage
from pages.checkout_p import checkOut, versand_button
from pages.checkout_p import new_adress
from pages.checkout_p import continue_button

import unittest

class TestCheckout(unittest.TestCase):
          baseURL = "https://dermalogica-staging.mindnow.io"
          driver = webdriver.Chrome("./driver/chromedriver 2")
          driver.implicitly_wait(3)
          driver.maximize_window()
          lp = loginPage(driver)
          ch = checkOut(driver)

          #check_order_page
          def test_checkout_flow(self):
              self.driver.get(self.baseURL)
              time.sleep(4)
              self.lp.login("Marijana@mindnow.io", "Mara1991")
              time.sleep(2)
              self.ch.put_product()
              time.sleep(2)
              self.ch.go_to_cart()

              if self.ch.isElementPresent(self, new_adress):
                  return self.ch.new_address_def('Marijana', 'Vukovic', 'Daniciceva', '3241', 'Belgrade', 'mara@mara.com','+3813423423' )
              elif self.ch.isElementPresent(self, versand_button):
                  return self.ch.click_on_versand()


              self.ch.continue_button_d()

              if self.ch.isElementPresent(self, versand_button):
                  return self.ch.click_on_versand()
              else:
                  self.ch.click_visa_card()



              self.ch.bestelung()
              result = self.ch.verifyCheckOrderSuccessful()
              assert result == True
              time.sleep(2)
              self.ch.bestelung()


#py.test -s -v tests/checkout_tests.py
