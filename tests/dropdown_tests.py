import time
import pytest
from selenium import webdriver
import unittest
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from pages.dropdown_page import Dropdown, fur_institute, fur_prof_institute, grundkonzepte, preisg_bildung, \
    rev_produkte, inov_produkte, buiss_unter
from pages.login_page import loginPage


class TestDropdown(unittest.TestCase):
    baseURL = "https://dermalogica-staging.mindnow.io"
    driver = webdriver.Chrome("./driver/chromedriver 2")
    driver.implicitly_wait(3)
    driver.maximize_window()
    lp = loginPage(driver)
    dropD = Dropdown(driver)

    @pytest.mark.run(order=1)
    def test_dropdown(self):
        self.driver.get(self.baseURL)
        time.sleep(3)
        self.lp.login("Marijana@mindnow.io", "Mara1991")
        time.sleep(2)
        action = ActionChains(self.driver)

        firstLevelMenu = self.driver.find_element(By.XPATH, fur_institute)
        action.move_to_element(firstLevelMenu).perform()

        secondLevelMenu = self.driver.find_element(By.XPATH, fur_prof_institute)
        secondLevelMenu2 = self.driver.find_element(By.XPATH, grundkonzepte)
        secondLevelMenu3 = self.driver.find_element(By.XPATH, preisg_bildung)
        secondLevelMenu4 = self.driver.find_element(By.XPATH, rev_produkte)
        secondLevelMenu5 = self.driver.find_element(By.XPATH, inov_produkte)
        secondLevelMenu6 = self.driver.find_element(By.XPATH, buiss_unter)


        line = [secondLevelMenu, secondLevelMenu2, secondLevelMenu3, secondLevelMenu4, secondLevelMenu5, secondLevelMenu6]
        for option in line:
         firstLevelMenu = self.driver.find_element(By.XPATH, fur_institute)
         action.move_to_element(firstLevelMenu).perform()
         option.click()


        time.sleep(4)

    @pytest.mark.run(order=2)
    def test_institut_finder(self,):
        self.driver.get(self.baseURL)
        time.sleep(3)
        self.dropD.finder("Skin Lounge")


# py.test -s -v tests/dropdown_tests.py



