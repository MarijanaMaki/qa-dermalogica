import time

import pytest
from selenium import webdriver
from pages.login_page import loginPage
import unittest

class TestLogin(unittest.TestCase):
          baseURL = "https://dermalogica-staging.mindnow.io"
          driver = webdriver.Chrome("./driver/chromedriver 2")
          driver.implicitly_wait(3)
          driver.maximize_window()
          lp = loginPage(driver)

          @pytest.mark.run(order=2)
          def test_valid(self):
              self.driver.get(self.baseURL)
              time.sleep(3)
              self.lp.login("Marijana@mindnow.io", "Mara1991")
              time.sleep(2)

          @pytest.mark.run(order=1)
          def test_invalid(self):
             self.driver.get(self.baseURL)
             time.sleep(2)
             self.lp.login("marijanaAAmindnow.io", "MAKIMAKI")
             time.sleep(3)


#py.test -s -v tests/login_test.py
